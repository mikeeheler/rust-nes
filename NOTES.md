# Notes and general task list

* Where error handling is desired, return a `std::Result` together with a type
  that implements `std::error::Error`. 


# Hardware Emulation

https://raw.githubusercontent.com/schenkzoola/NES/main/NES-001%20Console/NES-001.pdf

## CPU

* 1 control pin (34)
* 16 address pins (4-19)
* 8 data pins (21-28)

### Pin Out

https://wiki.nesdev.org/w/index.php?title=CPU_ALL


* **01 - AD1** : Audio out (pulse)
* **02 - AD2** : Audio out (triangle, noise, DPCM)
* **03 - /RST** : Low = reset state, all pins except 2 are in high impedance
  state. When released, CPU executes code (`$FFFC`/`$FFFD`) after 6 M2 clocks.
* **04-19 - A00-A15** : 16-bit address bus. Holds the target address during
  entire read/write cycle. For reads, the value is read from data bus during φ2.
  For writes, the value appears on Dx on φ2.
* **20 - GND** : Ground circuit.
* **21-28 - D7-D0** : 8-bit data bus.
* **29 - CLK** : 21.47727 MHz (NTSC) input clock. Clock is divided by 12 to feed
  clock input φ0, which is inverted to form φ1, and inverted again to form φ2.
  φ1 is high during first phase of each CPU cycle, while φ2 is high during
  2nd phase.
* **30 - TST** : Normally grounded.
* **31 - M2** : "Signals ready" pin. Roughly corresponds to CPU input clock φ0.
  CPU cycles begin when M2 goes low.
* **32 - /IRQ** : Interrupt pin.
* **33 - /NMI** : Non-maskable interrupt pin.
* **34 - R/W** : Low is write-mode. Stays high/low during an entire read/write
  cycle.
* **35 - /OE2** : Controller port 2. Low = controller enabled.
* **36 - /OE1** : Controller port 1.
* **37-39 - OUT2-OUT0** : Controller output pins. $4016 output latch bits 0-2.
  **OUT0** is additionally used as a "strobe" signal on both controller ports.
* **40 - +5V** : Input voltage.

The NMI is edge-sensitive meaning that the NMI is triggered by the falling-edge
of its signal, while IRQ is level sensitive. This acts as a rudimentary priority
system -- if both are set, NMI will execute first followed by IRQ (if it
remains high after NMI has completed).

## CPU RAM / WRAM

* 11 address pins

The on-board CPU RAM reads only 11 bits off the address bus so bits 11-15 must
be ignored.

The CPU will set the address pins then read off the data bus.

## PPU

The RP2C07 chip overlaps 8 of the address pins with the data pins. The first
8 address pins are fed into a latch along with the ALE (address latch enable)
pin. The latch itself will store the data on the address pins, reading when
ALE is high and keeping the stored data fresh when ALE is low.

The latch's output is fed into the VRAM directly.

To read/write data:

**Cycle 1**:
  * Set address pins


## Mappers

### NROM

* [nesdev.org](https://wiki.nesdev.org/w/index.php?title=NROM)
* NROM-256 with 32 KiB PRG ROM and 8 KiB CHR ROM
* NROM-128 with 16 KiB PRG ROM and 8 KiB CHR ROM
* CPU $6000-$7FFF: Family Basic only: PRG RAM, mirrored as necessary to fill
  entire 8 KiB window, write protectable with an external switch.
* CPU $8000-$BFFF: First 16 KiB of ROM.
* CPU $C000-$FFFF: Last 16 KiB of ROM (NROM-256) or mirror of $8000-$BFFF
  (NROM-128).

### UxROM, BxROM

