mod game_pak;
mod ines_error;
mod nrom;
mod pak_map;

pub use game_pak::GamePak;

use nrom::NROM;
use pak_map::PakMap;

use super::Machine;
