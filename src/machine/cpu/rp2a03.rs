// reference
// https://www.masswerk.at/6502/6502_instruction_set.html

use std::convert::From;
use std::mem::size_of;
use std::num::Wrapping;

use crate::machine::*;

use super::flags::*;

use super::CPUState;
use super::CPUStatus;
use super::Op;

use super::OPCODES;

const NMI_VECTOR_ADDRESS: u16 = 0xFFFA;
const RESET_VECTOR_ADDRESS: u16 = 0xFFFC;
const IRQ_VECTOR_ADDRESS: u16 = 0xFFFE;
const STACK_BASE: u16 = 0x0100;


/// RP2A03 Emulation
#[derive(Debug)]
pub struct RP2A03 {
    pub irq: bool,
    pub nmi: bool,

    state: CPUState,
    status: CPUStatus,
    cycle: u128,

    current_cycles: u8,
    current_op: &'static Op,
    fetch_addr: u16,

    last_instruction: Vec<u8>,
}

impl Default for RP2A03 {
    fn default() -> Self {
        RP2A03 {
            state: CPUState::default(),
            status: CPUStatus::default(),

            current_cycles: 0,
            current_op: &OPCODES[0xEA], // NOP
            cycle: 0,
            fetch_addr: 0,
            irq: false,
            nmi: false,

            last_instruction: vec![0],
        }
    }
}

// Public Interface #1
impl RP2A03 {
    pub fn accumulator(&self) -> u8 { self.state.accumulator }
    pub fn cycle_count(&self) -> u128 { self.cycle }
    pub fn program_counter(&self) -> u16 { self.state.program_counter }
    pub fn stack_pointer(&self) -> u8 { self.state.stack_pointer }
    pub fn status_register(&self) -> u8 { self.status.to_u8() }
    pub fn x_register(&self) -> u8 { self.state.x_register }
    pub fn y_register(&self) -> u8 { self.state.y_register }

    pub fn is_op_complete(&self) -> bool { self.current_cycles == 0 }

    pub fn set_program_counter(&mut self, address: u16) {
        self.state.program_counter = address;
    }
}

// Public Interface #2
impl RP2A03 {
    pub fn reset(&mut self, bus: &Bus) {
        // Reset is effectively an interrupt, however the write flag is forced
        // off for the duration, so nothing is written to the stack.
        // Nevertheless, the stack pointer is decremented for the PC and P
        // pushes and so it begins every reset at 0xFD rather than 0xFF as the
        // PC is fake-written to 0xFF and 0xFE, and P is fake-written to 0xFD.
        //
        self.state = CPUState {
            program_counter: bus.read_dword(RESET_VECTOR_ADDRESS).unwrap(),
            stack_pointer: 0xFD,
            ..CPUState::default()
        };
        self.status = CPUStatus {
            irq_disable: true,
            ..CPUStatus::default()
        };

        self.cycle = 7;
        self.current_cycles = 0;
        self.current_op = &OPCODES[0];
        self.fetch_addr = 0;
        self.irq = false;
        self.nmi = false;

        println!("CPU reset");
    }

    /// Execute a single cycle. This does not do full-cycle emulation, rather
    /// an instruction is loaded and executed and its cycle count is set to
    /// `current_cycles`. Subsequent calls to this function will no-op until
    /// the requested cycles have ticked down, and which point the next
    /// instruction is fetched and executed.
    pub fn tick(&mut self, bus: &mut Bus) {
        // The logic of this is a bit backwards and may lead to issues. For
        // better (but still not perfect) emulation, the loaded instruction
        // should not be executed until its cycle count has reached zero. There
        // may be cases where interactions with the PPU depend on intervening
        // PPU ticks execute before the instruction has completed, whereas here
        // it will happen after. Recall that the PPU ticks at 3x the speed of
        // the CPU.
        //
        if self.current_cycles == 0 {
            // The hardware interrupts act like the BRK command, except the PC
            // that they push to the stack is not incremented. BRK on the other
            // hand pushes PC+1 which has the result of returning the code to
            // the following byte, skipping an op.
            //
            // To emulate cycle counts as closely as possible, BRK is loaded as
            // the interrupts consume the same number of cycles.
            //
            // Ultimately they set the PC to some new address, and the code at
            // that address is expected to call RTI to bring us back.
            //
            if self.nmi {
                self.load_instruction(0);
                self.exec_interrupt(bus, 0, NMI_VECTOR_ADDRESS);
                self.nmi = false;
            }
            else if !self.status.irq_disable && self.irq {
                self.load_instruction(0);
                self.exec_interrupt(bus, 0, IRQ_VECTOR_ADDRESS);
                self.irq = false;
            }
            else {
                let opcode = self.read_param_word(bus);
                self.load_instruction(opcode);
                self.exec_next_instruction(bus);
            }
        }

        if self.current_cycles > 0 { self.current_cycles -= 1 }
        self.cycle += 1;
    }
}

// CPU state manipulation
//
impl RP2A03 {
    pub fn exec_branch(&mut self) {
        // If the new address crosses a page boundary, an additional cycle
        // is consumed.
        //
        if (self.state.program_counter & 0xFF00) != (self.fetch_addr & 0xFF00) {
            self.current_cycles += 1;
        }

        self.state.program_counter = self.fetch_addr;
    }

    pub fn exec_interrupt(&mut self, bus: &mut Bus, flags: u8, irq_vector: u16) {
        // This is invoked by /NMI, /IRQ, and BRK.
        // If invoked by BRK, the break flag will be set in the flags, otherwise
        // it will be zero.
        //
        self.push_stack_dword(bus, self.state.program_counter);
        self.push_stack_word(bus, self.status.to_u8() | flags);
        self.state.program_counter = bus.read_dword(irq_vector).unwrap();

        // It's important to set the IRQ disable flag after pushing the status
        // flags so that RTI can restore the flags with whatever state this was
        // in at the time the interrupt was invoked.
        //
        self.status.irq_disable = true;
    }

    pub fn exec_next_instruction(&mut self, bus: &mut Bus) {
        match self.current_op {
            Op::Branch(branch_fn, _) => {
                let _ = self.fetch_rel(bus);
                if branch_fn(self) {
                    // If a branch is taken, an additional cycle is consumed.
                    //
                    self.current_cycles += 1;
                    self.exec_branch();
                }
            },
            Op::Dynamic(exec_fn, fetch_fn, _) => {
                if fetch_fn(self, bus) {
                    // Add a cycle if the fetch function requires it.
                    //
                    self.current_cycles += 1;
                }
                exec_fn(self, bus);
            },
            Op::Implied(exec_fn, _) => {
                exec_fn(self, bus);
            },
            Op::Invalid() => panic!("CRASH"),
            Op::Static(exec_fn, fetch_fn, _) => {
                // Static ops don't increment the cycle count if a page boundary
                // was crossed, so the return value is discarded here.
                //
                let _ = fetch_fn(self, bus);
                exec_fn(self, bus);
            },
        }
    }

    /// Helper function. Literally STACK_BASE | stack_pointer.
    fn get_stack_address(&self) -> u16 {
        STACK_BASE | u16::from(self.state.stack_pointer)
    }

    /// Loads the opcode into state.instruction, the instruction profile into
    /// current_op, and sets current_cycles to the base cycle count of the
    /// opcode.
    fn load_instruction(&mut self, opcode: u8) {
        self.current_op = &OPCODES[usize::from(opcode)];
        self.current_cycles = match self.current_op {
            Op::Branch(_, cycles) => *cycles,
            Op::Dynamic(_, _, cycles) => *cycles,
            Op::Implied(_, cycles) => *cycles,
            Op::Invalid() => 0,
            Op::Static(_, _, cycles) => *cycles,
        };
    }

    fn read_param_dword(&mut self, bus: &Bus) -> u16 {
        let result = bus.read_dword(self.state.program_counter).unwrap();
        self.state.program_counter += size_of::<u16>() as u16;

        result
    }

    fn read_param_word(&mut self, bus: &Bus) -> u8 {
        let result = bus.read_word(self.state.program_counter).unwrap();
        self.state.program_counter += size_of::<u8>() as u16;

        result
    }

    /// Helper function. Sets the negative and zero flags for the input value.
    fn set_nz_flags(&mut self, data: u8) {
        self.status.negative = data & 0x80 == 0x80;
        self.status.zero = data == 0;
    }
}

// Stack functions
impl RP2A03 {
    /// Returns the dword from the top of the stack and increments the stack
    /// pointer by 2.
    fn pop_stack_dword(&mut self, bus: &Bus) -> u16 {
        assert!(self.state.stack_pointer < 0xFE, "Stack underflow.");
        let data = bus.read_dword(self.get_stack_address()).unwrap();
        self.state.stack_pointer += 2;

        data
    }

    /// Returns the word from the top of the stack and increments the stack
    /// pointer by 1.
    fn pop_stack_word(&mut self, bus: &Bus) -> u8 {
        assert!(self.state.stack_pointer < 0xFF, "Stack underflow.");
        let data = bus.read_word(self.get_stack_address()).unwrap();
        self.state.stack_pointer += 1;

        data
    }

    /// Decrements the stack pointer by 2 and writes `data` to the top of the
    /// stack.
    fn push_stack_dword(&mut self, bus: &mut Bus, data: u16) {
        assert!(self.state.stack_pointer > 1, "Stack overflow.");
        self.state.stack_pointer -= 2;
        bus.write_dword(self.get_stack_address(), data).unwrap();
    }

    /// Decrements the stack pointer by 1 and writes `data` to the top of the
    /// stack.
    fn push_stack_word(&mut self, bus: &mut Bus, data: u8) {
        assert!(self.state.stack_pointer > 0, "Stack overflow.");
        self.state.stack_pointer -= 1;
        bus.write_word(self.get_stack_address(), data).unwrap();
    }
}

// Fetch Functions
impl RP2A03 {
    /// Absolute. Reads a dword from program_counter and sets fetch_addr to the
    /// result.
    ///
    /// Syntax: `$0000`
    pub fn fetch_abs(&mut self, bus: &Bus) -> bool {
        self.fetch_addr = self.read_param_dword(bus);

        false
    }

    /// Absolute,X. Reads a dword from program_counter and sets fetch_addr to
    /// the result, offset by the X register. Returns true if a page boundary
    /// was crossed as a result of adding X.
    ///
    /// Syntax: `$0000,X`
    pub fn fetch_abx(&mut self, bus: &Bus) -> bool {
        let base_addr = self.read_param_dword(bus);
        self.fetch_addr = base_addr + u16::from(self.state.x_register);

        let page_crossed = (self.fetch_addr & 0xFF00) != (base_addr & 0xFF00);
        page_crossed
    }

    /// Absolute,Y. Reads a dword from program_counter and sets fetch_addr to
    /// the result, offset by the Y register. Returns true if a page boundary
    /// was crossed as a result of adding Y.
    ///
    /// Syntax: `$0000,Y`
    pub fn fetch_aby(&mut self, bus: &Bus) -> bool {
        let base_addr = self.read_param_dword(bus);
        self.fetch_addr = base_addr + u16::from(self.state.y_register);

        let page_crossed = (self.fetch_addr  & 0xFF00) != (base_addr & 0xFF00);
        page_crossed
    }

    /// Immediate. Sets fetch_addr to the program counter and increments the
    /// program counter by 1.
    ///
    /// Syntax: `#$00`
    pub fn fetch_imm(&mut self, _: &Bus) -> bool {
        self.fetch_addr = self.state.program_counter;
        self.state.program_counter += 1;

        false
    }

    /// Implied. No state is altered, the associated op does not read or write
    /// memory and only operates on internal CPU registers.
    pub fn fetch_imp(&mut self, _: &Bus) -> bool {
        false
    }

    /// Indirect. Reads a dword from program_counter and sets fetch_addr to
    /// the address pointed to by the data at that location. This addressing
    /// mode is only used by the `JMP` instruction.
    ///
    /// There is a hardware bug on the 6502 which is emulated here in that if
    /// the dword crosses a page boundary (i.e. `JMP ($12FF)`), the low byte
    /// will be fetched from `$12FF` and the high byte from `$1200`.
    ///
    /// Syntax: `($0000)`
    pub fn fetch_ind(&mut self, bus: &Bus) -> bool {
        let ptr = self.read_param_dword(bus);

        // TODO this may not behave correctly if ptr is 0xFFFF.
        let hi_addr = ((ptr + 1) & 0x00FF) | (ptr & 0xFF00);
        let lo = bus.read_word(ptr).unwrap();
        let hi = bus.read_word(hi_addr).unwrap();
        self.fetch_addr = u16::from_le_bytes([lo, hi]);

        false
    }

    /// X,Indirect. Reads a word from program_counter and sets fetch_addr to
    /// the address pointed to by the dword at $00BB+X. Addresses are limited to
    /// the zero page and will wrap if $00BB+X would cross the page boundary.
    ///
    /// There is a hardware bug on the 6502 which is emulated here in that if
    /// the dword crosses the zero-page boundary (i.e. `LDX #$00, LDA ($FF,X)`),
    /// the low byte will be fetched from $00FF and the high byte from $0000.
    ///
    /// Syntax: `($00,X)`
    pub fn fetch_izx(&mut self, bus: &Bus) -> bool {
        let ptr_lo = self.read_param_word(bus);

        let lo_addr = ptr_lo.wrapping_add(self.state.x_register);
        let hi_addr = u16::from(lo_addr.wrapping_add(1));
        let lo = bus.read_word(u16::from(ptr_lo)).unwrap();
        let hi = bus.read_word(hi_addr).unwrap();
        self.fetch_addr = u16::from_le_bytes([lo, hi]);

        false
    }

    /// Indirect,Y. Reads a word from program_counter and sets fetch_addr to the
    /// address pointed to by $00BB, with Y as an offset. Should the offset
    /// address cross a page boundary from the base address, an additional cycle
    /// will be required.
    ///
    /// There is a hardware bug on the 6502 which is emulated here in that if
    /// the dword crosses the zero-page boundary (i.e. `LDA ($FF),Y`), the low
    /// byte will be fetched from $00FF and the high byte from $0000.
    ///
    /// Syntax: `($00),Y`
    pub fn fetch_izy(&mut self, bus: &Bus) -> bool {
        let ptr_lo = self.read_param_word(bus);

        let lo_addr = u16::from(ptr_lo); // $00LL
        let hi_addr = u16::from(ptr_lo.wrapping_add(1)); // $00LL+1 (FF -> 00)
        let lo = bus.read_word(lo_addr).unwrap();
        let hi = bus.read_word(hi_addr).unwrap();
        let base_addr = u16::from_le_bytes([lo, hi]);
        self.fetch_addr = base_addr + u16::from(self.state.y_register);

        let page_cross = (self.fetch_addr  & 0xFF00) != (base_addr & 0xFF00);
        page_cross
    }

    /// Relative. Reads a word from program_counter and sets fetch_addr to the
    /// result of adding that word, interpreted as a signed 8-bit integer, to
    /// the program couter.
    ///
    /// Only used by branching instructions.
    ///
    /// Syntax: `BNE #$80`
    pub fn fetch_rel(&mut self, bus: &Bus) -> bool {
        let data = self.read_param_word(bus);

        // This might seem a bit obtuse. The fetched data, a u8, is cast to an
        // i8. It's a straight cast, so 128u8 will cast correctly to -128i8.
        //
        // Next, the i8 is cast to a u16, which performs sign extension, so:
        //   -128i8 = 0x80 = 0b1000_0000
        //   (-128i8 as u16) = 0xFF80 = 0b1111_1111_1000_0000
        // wrapping_add ensures that the add is wrapped rather than crash with
        // an overflow panic. The result is effectively a subtraction for
        // negative numbers.
        //
        // Another way to do this might be to cast everything up to i32, do the
        // path, then cast it back, but this seemed cleaner.
        //
        // Also StackOverflow said to do this.
        //
        let offset = data as i8;
        self.fetch_addr = self.state.program_counter.wrapping_add(offset as u16);

        false
    }

    /// Zeropage. Reads a word from program_counter and sets fetch_addr to
    /// $00BB.
    pub fn fetch_zp(&mut self, bus: &Bus) -> bool {
        self.fetch_addr = u16::from(self.read_param_word(bus));

        false
    }

    /// Zeropage, X-Indexed. Reads a word from program_counter and sets
    /// fetch_addr to the zero-page address at that location, plus X.
    ///
    /// fetch_addr will always be in the zeropage. If the arithmetic wraps
    /// around, an additional cycle is required.
    pub fn fetch_zpx(&mut self, bus: &Bus) -> bool {
        let addr = self.read_param_word(bus);

        let offset_addr = addr.wrapping_add(self.state.x_register);
        self.fetch_addr = u16::from(offset_addr);

        let page_cross = offset_addr < addr;
        page_cross
    }

    /// Zeropage, Y-Indexed. Reads a word from program_counter and sets
    /// fetch_addr to the zero-page address at that location, plus X.
    ///
    /// fetch_addr will always be in the zeropage. If the arithmetic wraps
    /// around, an additional cycle is required.
    pub fn fetch_zpy(&mut self, bus: &Bus) -> bool {
        let addr = self.read_param_word(bus);

        let offset_addr = addr.wrapping_add(self.state.y_register);
        self.fetch_addr = u16::from(offset_addr);

        let page_cross = offset_addr < addr;
        page_cross
    }
}

// Branch Instructions
impl RP2A03 {
    /// BCC - branch on carry clear
    pub fn exec_bcc(&mut self) -> bool {
        self.status.carry == false
    }

    /// BCS - branch on carry set
    pub fn exec_bcs(&mut self) -> bool {
        self.status.carry
    }

    /// BEQ - branch on equal
    pub fn exec_beq(&mut self) -> bool {
        self.status.zero
    }

    /// BMI - branch on minus (negative set)
    pub fn exec_bmi(&mut self) -> bool {
        self.status.negative
    }

    /// BNE - branch on not equal (zero clear)
    pub fn exec_bne(&mut self) -> bool {
        self.status.zero == false
    }

    /// BPL - branch on plus (negative clear)
    pub fn exec_bpl(&mut self) -> bool {
        self.status.negative == false
    }

    /// BVC - branch on overflow clear
    pub fn exec_bvc(&mut self) -> bool {
        self.status.overflow == false
    }

    /// BVS - branch on overflow set
    pub fn exec_bvs(&mut self) -> bool {
        self.status.overflow
    }
}

// Instructions
impl RP2A03 {
    /// ADC - add with carry
    pub fn exec_adc(&mut self, bus: &mut Bus) {
        let input = bus.read_word(self.fetch_addr).unwrap();
        let a = self.state.accumulator;
        let carry_bit: u16 = if self.status.carry { 1 } else { 0 };
        let sum = u16::from(a) + u16::from(input) + carry_bit;
        let s = sum as u8;

        // The overflow flag is set when the addition would result in a signed
        // arithmetic overflow. This happens one of two ways: two positives
        // result in a negative, or two negatives result in a positive.
        //
        // We can determine this by XORing the sign bit of each input with the
        // result, then anding the result of that. A truth table is helpful here
        // (a=accumulator, i=input, s=sum, v=overflow):
        //
        //      a i s | v | a^s | i^s | (a^s)&(i^s)
        //      0 0 0 | 0 |  0  |  0  |      0      ; p+p=p
        //      0 0 1 | 1 |  1  |  1  |      1      ; p+p=n*
        //      0 1 0 | 0 |  0  |  1  |      0      ; p+n=p
        //      0 1 1 | 0 |  1  |  0  |      0      ; p+n=n
        //      1 0 0 | 0 |  1  |  0  |      0      ; n+p=p
        //      1 0 1 | 0 |  0  |  0  |      0      ; n+p=n
        //      1 1 0 | 1 |  1  |  1  |      1      ; n+n=p*
        //      1 1 1 | 0 |  0  |  0  |      0      ; n+n=n
        //
        // The result is also AND'd with 0x80 to isolate the sign bit.
        //
        self.status.overflow = (a ^ s) & (input ^ s) & 0x80 == 0x80;
        self.status.carry = sum & 0x100 == 0x100;
        self.status.negative = s & 0x80 == 0x80;
        self.status.zero = s == 0;
        self.state.accumulator = s;
    }

    /// AND - and (with accumulator)
    pub fn exec_and(&mut self, bus: &mut Bus) {
        let data = bus.read_word(self.fetch_addr).unwrap();
        self.state.accumulator &= data;
        self.set_nz_flags(self.state.accumulator);
    }

    /// ASL.A - arithmetic shift left accumulator
    pub fn exec_asl_a(&mut self, _: &mut Bus) {
        self.status.carry = self.state.accumulator & 0x80 == 0x80;
        self.state.accumulator = self.state.accumulator << 1;
        self.set_nz_flags(self.state.accumulator);
    }

    /// ASL - arithmetic shift left
    pub fn exec_asl(&mut self, bus: &mut Bus) {
        let mut input = bus.read_word(self.fetch_addr).unwrap();
        self.status.carry = input & 0x80 == 0x80;
        input <<= 1;
        self.set_nz_flags(input);
        bus.write_word(self.fetch_addr, input).unwrap();
    }

    /// BIT - bit test
    pub fn exec_bit(&mut self, bus: &mut Bus) {
        let data = bus.read_word(self.fetch_addr).unwrap();
        self.status.overflow = data & OVERFLOW_MASK == OVERFLOW_MASK;
        self.status.negative = data & NEGATIVE_MASK == NEGATIVE_MASK;
        self.status.zero = self.state.accumulator & data == 0;
    }

    /// BRK - break / interrupt
    pub fn exec_brk(&mut self, bus: &mut Bus) {
        self.state.program_counter += 1;
        self.exec_interrupt(bus, BREAK_MASK, IRQ_VECTOR_ADDRESS);
    }

    /// CLC - clear carry
    pub fn exec_clc(&mut self, _: &mut Bus) {
        self.status.carry = false;
    }

    /// CLD - clear decimal
    pub fn exec_cld(&mut self, _: &mut Bus) {
        self.status.decimal = false;
    }

    /// CLI - clear interrupt disable
    pub fn exec_cli(&mut self, _: &mut Bus) {
        self.status.irq_disable = false;
    }

    /// CLV - clear overflow
    pub fn exec_clv(&mut self, _: &mut Bus) {
        self.status.overflow = false;
    }

    /// CMP - compare (with accumulator)
    pub fn exec_cmp(&mut self, bus: &mut Bus) {
        let m = bus.read_word(self.fetch_addr).unwrap();
        let a = self.state.accumulator;
        let r = (Wrapping(a) - Wrapping(m)).0;

        self.status.carry = a >= m;
        self.status.negative = r & 0x80 == 0x80;
        self.status.zero = a == m;
    }

    /// CPX - compare with X
    pub fn exec_cpx(&mut self, bus: &mut Bus) {
        let m = bus.read_word(self.fetch_addr).unwrap();
        let x = self.state.x_register;
        let r = (Wrapping(x) - Wrapping(m)).0;

        self.status.carry = x >= m;
        self.status.negative = r & 0x80 == 0x80;
        self.status.zero = x == m;
    }

    /// CPY - compare with Y
    pub fn exec_cpy(&mut self, bus: &mut Bus) {
        let m = bus.read_word(self.fetch_addr).unwrap();
        let y = self.state.y_register;
        let r = (Wrapping(y) - Wrapping(m)).0;

        self.status.carry = y >= m;
        self.status.negative = r & 0x80 == 0x80;
        self.status.zero = y == m;
    }

    /// DEC - decrement
    pub fn exec_dec(&mut self, bus: &mut Bus) {
        let x = Wrapping(bus.read_word(self.fetch_addr).unwrap());
        let one = Wrapping(1u8);
        let result = (x - one).0;
        bus.write_word(self.fetch_addr, result).unwrap();
        self.set_nz_flags(result);
    }

    /// DEX - decrement X
    pub fn exec_dex(&mut self, _: &mut Bus) {
        let x = Wrapping(self.state.x_register);
        let one = Wrapping(1u8);
        self.state.x_register = (x - one).0;
        self.set_nz_flags(self.state.x_register);
    }

    /// DEY - decrement Y
    pub fn exec_dey(&mut self, _: &mut Bus) {
        let y = Wrapping(self.state.y_register);
        let one = Wrapping(1u8);
        self.state.y_register = (y - one).0;
        self.set_nz_flags(self.state.y_register);
    }

    /// EOR - exclusive OR (with accumulator)
    pub fn exec_eor(&mut self, bus: &mut Bus) {
        let data = bus.read_word(self.fetch_addr).unwrap();
        self.state.accumulator ^= data;
        self.set_nz_flags(self.state.accumulator);
    }

    /// INC - increment
    pub fn exec_inc(&mut self, bus: &mut Bus) {
        let x = Wrapping(bus.read_word(self.fetch_addr).unwrap());
        let one = Wrapping(1u8);
        let result = (x + one).0;
        bus.write_word(self.fetch_addr, result).unwrap();
        self.set_nz_flags(result);
    }

    /// INX - increment X
    pub fn exec_inx(&mut self, _: &mut Bus) {
        let x = Wrapping(self.state.x_register);
        let one = Wrapping(1u8);
        self.state.x_register = (x + one).0;
        self.set_nz_flags(self.state.x_register);
    }

    /// INY - increment Y
    pub fn exec_iny(&mut self, _: &mut Bus) {
        let y = Wrapping(self.state.y_register);
        let one = Wrapping(1u8);
        self.state.y_register = (y + one).0;
        self.set_nz_flags(self.state.y_register);
    }

    /// JMP - jump
    pub fn exec_jmp(&mut self, _: &mut Bus) {
        self.state.program_counter = self.fetch_addr;
    }

    /// JSR - jump subroutine
    pub fn exec_jsr(&mut self, bus: &mut Bus) {
        self.push_stack_dword(bus, self.state.program_counter);
        self.state.program_counter = self.fetch_addr;
    }

    /// LDA - load accumulator
    pub fn exec_lda(&mut self, bus: &mut Bus) {
        self.state.accumulator = bus.read_word(self.fetch_addr).unwrap();
        self.set_nz_flags(self.state.accumulator);
    }

    /// LDX - load X
    pub fn exec_ldx(&mut self, bus: &mut Bus) {
        self.state.x_register = bus.read_word(self.fetch_addr).unwrap();
        self.set_nz_flags(self.state.x_register);
    }

    /// LDY - load Y
    pub fn exec_ldy(&mut self, bus: &mut Bus) {
        self.state.y_register = bus.read_word(self.fetch_addr).unwrap();
        self.set_nz_flags(self.state.y_register);
    }

    /// LSR.A - logical shift right accumulator
    pub fn exec_lsr_a(&mut self, _: &mut Bus) {
        self.status.carry = self.state.accumulator & 1 == 1;
        self.state.accumulator >>= 1;
        self.status.negative = false;
        self.set_nz_flags(self.state.accumulator);
    }

    /// LSR - logical shift right
    pub fn exec_lsr(&mut self, bus: &mut Bus) {
        let mut data = bus.read_word(self.fetch_addr).unwrap();
        self.status.carry = data & 1 == 1;
        data >>= 1;
        bus.write_word(self.fetch_addr, data).unwrap();
        self.status.negative = false;
        self.set_nz_flags(data);
    }

    /// NOP - no operation
    pub fn exec_nop(&mut self, _: &mut Bus) {
        // nothing to do
    }

    /// ORA - or with accumulator
    pub fn exec_ora(&mut self, bus: &mut Bus) {
        let data = bus.read_word(self.fetch_addr).unwrap();
        self.state.accumulator |= data;
        self.set_nz_flags(self.state.accumulator);
    }

    /// PHA - push accumulator
    pub fn exec_pha(&mut self, bus: &mut Bus) {
        self.push_stack_word(bus, self.state.accumulator);
    }

    /// PHP - push processor status (SR)
    pub fn exec_php(&mut self, bus: &mut Bus) {
        self.push_stack_word(bus, self.status.to_u8());
    }

    /// PLA - pull accumulator
    pub fn exec_pla(&mut self, bus: &mut Bus) {
        self.state.accumulator = self.pop_stack_word(bus);
        self.set_nz_flags(self.state.accumulator);
    }

    /// PLP - pull processor status (SR)
    pub fn exec_plp(&mut self, bus: &mut Bus) {
        let flags = self.pop_stack_word(bus);
        self.status = CPUStatus::from_u8(flags);
    }

    /// ROL.A - rotate left accumulator
    pub fn exec_rol_a(&mut self, _: &mut Bus) {
        let carry_bit: u8 = if self.status.carry { 1 } else { 0 };
        self.status.carry = self.state.accumulator & 0x80 == 0x80;
        self.state.accumulator = (self.state.accumulator << 1) | carry_bit;
        self.set_nz_flags(self.state.accumulator);
    }

    /// ROL - rotate left
    pub fn exec_rol(&mut self, bus: &mut Bus) {
        let mut data = bus.read_word(self.fetch_addr).unwrap();
        let carry_bit: u8 = if self.status.carry { 1 } else { 0 };
        self.status.carry = data & 0x80 == 0x80;
        data = (data << 1) | carry_bit;
        bus.write_word(self.fetch_addr, data).unwrap();
        self.set_nz_flags(data);
    }

    /// ROR.A - rotate right accumulator
    pub fn exec_ror_a(&mut self, _: &mut Bus) {
        let carry_bit: u8 = if self.status.carry { 0x80 } else { 0 };
        self.status.negative = self.status.carry;
        self.status.carry = self.state.accumulator & 1 == 1;
        self.state.accumulator = (self.state.accumulator >> 1) | carry_bit;
        self.status.zero = self.state.accumulator == 0;
    }

    /// ROR - rotate right
    pub fn exec_ror(&mut self, bus: &mut Bus) {
        let mut data = bus.read_word(self.fetch_addr).unwrap();
        let carry_bit: u8 = if self.status.carry { 0x80 } else { 0 };
        self.status.negative = self.status.carry;
        self.status.carry = data & 1 == 1;
        data = (data >> 1) | carry_bit;
        self.status.zero = data == 0;
        bus.write_word(self.fetch_addr, data).unwrap();
    }

    /// RTI - return from interrupt
    pub fn exec_rti(&mut self, bus: &mut Bus) {
        self.status = CPUStatus::from_u8(self.pop_stack_word(bus));
        self.state.program_counter = self.pop_stack_dword(bus);
    }

    /// RTS - return from subroutine
    pub fn exec_rts(&mut self, bus: &mut Bus) {
        self.state.program_counter = self.pop_stack_dword(bus);
    }

    /// SBC - subtract with carry
    pub fn exec_sbc(&mut self, bus: &mut Bus) {
        let data = bus.read_word(self.fetch_addr).unwrap();
        let a = self.state.accumulator;

        // Subtract is the same as add with a negative numbers. A two's
        // complement negate is done by flipping the bits (XOR 0xFF) and adding
        // one.
        //
        //     LDA #$80         LDA #$80
        //     SEC              CLC
        //     SBC #$5          SBC #$5
        //     A=0x7B, nVzC     A=0x7A, nVzC
        //
        // This can be short-circuited thanks to maths, and all we really need
        // to do is flip the bits of the input and perform a standard add with
        // carry.
        //

        let carry_bit: u16 = if self.status.carry { 1 } else { 0 };
        let input = data ^ 0xFF;

        let sum = u16::from(a) + u16::from(input) + carry_bit;
        let s = sum as u8;

        // See the note about overflow in exec_adc.
        //
        self.status.overflow = (a ^ s) & (input ^ s) & 0x80 == 0x80;
        self.status.carry = sum & 0x100 == 0x100;
        self.status.negative = s & 0x80 == 0x80;
        self.status.zero = s == 0;
        self.state.accumulator = s;
    }

    /// SEC - set carry
    pub fn exec_sec(&mut self, _: &mut Bus) {
        self.status.carry = true;
    }

    /// SED - set decimal
    pub fn exec_sed(&mut self, _: &mut Bus) {
        self.status.decimal = true;
    }

    /// SEI - set interrupt disable
    pub fn exec_sei(&mut self, _: &mut Bus) {
        self.status.irq_disable = true;
    }

    /// STA - store acccumulator
    pub fn exec_sta(&mut self, bus: &mut Bus) {
        bus.write_word(self.fetch_addr, self.state.accumulator).unwrap();
    }

    /// STX - store X
    pub fn exec_stx(&mut self, bus: &mut Bus) {
        bus.write_word(self.fetch_addr, self.state.x_register).unwrap();
    }

    /// STY - store Y
    pub fn exec_sty(&mut self, bus: &mut Bus) {
        bus.write_word(self.fetch_addr, self.state.y_register).unwrap();
    }

    /// TAX - transfer accumulator to X
    pub fn exec_tax(&mut self, _: &mut Bus) {
        self.state.x_register = self.state.accumulator;
        self.set_nz_flags(self.state.x_register);
    }

    /// TAY - transfer accumulator to Y
    pub fn exec_tay(&mut self, _: &mut Bus) {
        self.state.y_register = self.state.accumulator;
        self.set_nz_flags(self.state.y_register);
    }

    /// TSX - transfer stack pointer to X
    pub fn exec_tsx(&mut self, _: &mut Bus) {
        self.state.x_register = self.state.stack_pointer;
        self.set_nz_flags(self.state.x_register);
    }

    /// TXA - transfer X to accumulator
    pub fn exec_txa(&mut self, _: &mut Bus) {
        self.state.accumulator = self.state.x_register;
        self.set_nz_flags(self.state.accumulator);
    }

    /// TXS - transfer X to stack pointer
    pub fn exec_txs(&mut self, _: &mut Bus) {
        self.state.stack_pointer = self.state.x_register;
    }

    /// TYA - transfer Y to accumulator
    pub fn exec_tya(&mut self, _: &mut Bus) {
        self.state.accumulator = self.state.y_register;
        self.set_nz_flags(self.state.accumulator);
    }
}

// Hidden / Illegal Instructions
impl RP2A03 {

    /// SLO - ASL + ORA
    pub fn exec_slo(&mut self, bus: &mut Bus) {
        self.exec_asl(bus);
        self.exec_ora(bus);
    }

    /// RLA - ROL + AND
    pub fn exec_rla(&mut self, bus: &mut Bus) {
        self.exec_rol(bus);
        self.exec_and(bus);
    }

    /// SRE - LSR + EOR
    pub fn exec_sre(&mut self, bus: &mut Bus) {
        self.exec_lsr(bus);
        self.exec_eor(bus);
    }

    /// RRA - ROR + ADC
    pub fn exec_rra(&mut self, bus: &mut Bus) {
        self.exec_ror(bus);
        self.exec_adc(bus);
    }

    /// SAX - {adr} := A & X
    pub fn exec_sax(&mut self, bus: &mut Bus) {
        let data = self.state.accumulator & self.state.x_register;
        bus.write_word(self.fetch_addr, data).unwrap();
    }

    /// LAX - LDA + LDX
    pub fn exec_lax(&mut self, bus: &mut Bus) {
        self.exec_lda(bus);
        self.exec_ldx(bus);
    }

    /// DCP - DEC + CMP
    pub fn exec_dcp(&mut self, bus: &mut Bus) {
        self.exec_dec(bus);
        self.exec_cmp(bus);
    }

    /// ISC - INC + SBC
    pub fn exec_isc(&mut self, bus: &mut Bus) {
        self.exec_inc(bus);
        self.exec_sbc(bus);
    }

    /// ANC - A := A & {adr}; carry = A bit 7
    pub fn exec_anc(&mut self, bus: &mut Bus) {
        let data = bus.read_word(self.fetch_addr).unwrap();
        self.state.accumulator &= data;
        self.status.carry = data & 0x80 == 0x80;
        self.set_nz_flags(self.state.accumulator);
    }

    /// ALR - AND #{imm} + LSR
    pub fn exec_alr(&mut self, _: &mut Bus) {
    }

    /// ARR - AND #{imm} + ROR
    pub fn exec_arr(&mut self, _: &mut Bus) {
    }

    /// XAA - TXA + AND #{imm}
    ///
    /// Highly unstable. On actual hardware the CPU may lock up.
    pub fn exec_xaa(&mut self, _: &mut Bus) {
    }

    /// AXS - A&X minus #{imm} into X
    pub fn exec_axs(&mut self, _: &mut Bus) {
    }

    /// AHX - {adr} := A&X&H
    pub fn exec_ahx(&mut self, _: &mut Bus) {
    }

    /// SHY - {adr} := Y&H
    pub fn exec_shy(&mut self, _: &mut Bus) {
    }

    /// SHX - {adr} := X&H
    pub fn exec_shx(&mut self, _: &mut Bus) {
    }

    /// TAS - S := A&X, {adr} := S&H
    pub fn exec_tas(&mut self, _: &mut Bus) {
    }

    /// LAS - A,X,S := {adr} & S
    pub fn exec_las(&mut self, _: &mut Bus) {
    }
}
