pub const CARRY_MASK: u8 = 1 << 0;
pub const ZERO_MASK: u8 = 1 << 1;
pub const INTERRUPT_MASK: u8 = 1 << 2;
pub const DECIMAL_MASK: u8 = 1 << 3;
pub const BREAK_MASK: u8 = 1 << 4;
pub const UNUSED_MASK: u8 = 1 << 5;
pub const OVERFLOW_MASK: u8 = 1 << 6;
pub const NEGATIVE_MASK: u8 = 1 << 7;
