use std::fmt;

use super::{BranchFn, ExecFn, FetchFn};

#[derive(Copy, Clone)]
pub enum Op {
    /// Branching op.
    Branch(BranchFn, u8),

    /// Dynamic op with a cycle count that may increase from the base.
    Dynamic(ExecFn, FetchFn, u8),

    /// Standard op with no parameters.
    Implied(ExecFn, u8),

    /// An op that halts the CPU completely.
    Invalid(),

    /// A standard, documented 6502 op with a static cycle count.
    Static(ExecFn, FetchFn, u8),
}

impl fmt::Debug for Op {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        match self {
            Op::Branch(_, cycles) => write!(f, "Branch({})", cycles),
            Op::Dynamic(_, _, cycles) => write!(f, "Dynamic({})", cycles),
            Op::Implied(_, cycles) => write!(f, "Implied({})", cycles),
            Op::Invalid() => write!(f, "Invalid"),
            Op::Static(_, _, cycles) => write!(f, "Static({})", cycles)
        }
    }
}

impl fmt::Display for Op {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "{:?}", self)
    }
}
