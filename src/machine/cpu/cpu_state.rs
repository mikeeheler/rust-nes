/// CPU Registers
#[derive(Default, Copy, Clone, Debug)]
pub struct CPUState {
    pub program_counter: u16,
    pub accumulator: u8,
    pub x_register: u8,
    pub y_register: u8,
    pub stack_pointer: u8,
}
