use super::flags::*;

/// Status Register (6 flags)
#[derive(Default, Copy, Clone, Debug)]
pub struct CPUStatus {
    pub carry: bool,
    pub zero: bool,
    pub irq_disable: bool,
    pub decimal: bool,
    pub overflow: bool,
    pub negative: bool,
}

impl CPUStatus {
    pub fn from_u8(flags: u8) -> Self {
        Self {
            carry: flags & CARRY_MASK == CARRY_MASK,
            zero: flags & ZERO_MASK == ZERO_MASK,
            irq_disable: flags & INTERRUPT_MASK == INTERRUPT_MASK,
            decimal: flags & DECIMAL_MASK == DECIMAL_MASK,
            overflow: flags & OVERFLOW_MASK == OVERFLOW_MASK,
            negative: flags & NEGATIVE_MASK == NEGATIVE_MASK,
        }
    }

    pub fn to_u8(&self) -> u8 {
        let mut result = UNUSED_MASK;

        if self.carry { result |= CARRY_MASK }
        if self.zero { result |= ZERO_MASK }
        if self.irq_disable { result |= INTERRUPT_MASK }
        if self.decimal { result |= DECIMAL_MASK }
        if self.overflow { result |= OVERFLOW_MASK }
        if self.negative { result |= NEGATIVE_MASK }

        result
    }
}
