mod cpu_state;
mod cpu_status;
mod flags;
mod op;
mod opcodes;
mod rp2a03;

pub use rp2a03::RP2A03;

use cpu_state::CPUState;
use cpu_status::CPUStatus;
use op::Op;
use opcodes::OPCODES;
use super::Bus;

type BranchFn = fn(&mut RP2A03) -> bool;
type ExecFn = fn(&mut RP2A03, &mut Bus);
type FetchFn = fn(&mut RP2A03, &Bus) -> bool;
