use super::Bus;
use super::RP2A03;
use super::PPU;
use super::RP2C02;

#[derive(Debug, Default)]
pub struct Machine<'a> {
    pub bus: Bus<'a>,
    pub cpu: RP2A03,
    pub ppu: RP2C02,
}

impl Machine<'_> {
    pub fn new() -> Self { Self::default() }

    pub fn reset(&mut self) -> Result<(), MachineError> {
        self.bus.reset();
        self.cpu.reset(&self.bus);
        self.ppu.reset();

        Ok(())
    }
}

#[derive(Debug)]
pub enum MachineError {
    GamePakError,
    CPUError,
    PPUError,
}
