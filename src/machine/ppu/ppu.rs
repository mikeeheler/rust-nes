use std::fmt::Debug;

pub trait PPU: Debug + Default {
    fn reset(&mut self);
    fn tick(&mut self);
}
