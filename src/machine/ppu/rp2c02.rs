use super::PPU;
use std::fmt;

#[derive(Debug, Default)]
pub struct RP2C02 {
    pub x: i16,
    pub y: i16,

    pub cycle: u64,
    pub frame: u64,
    pub frame_complete: bool,
    pub odd_frame: bool,

    pub vblank: bool,
    pub sprite0: bool,
    pub overflow: bool,
}

impl fmt::Display for RP2C02 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "{: >3},{: >3}", self.y, self.x)
    }
}

impl PPU for RP2C02 {
    fn reset(&mut self) {
        self.x = 0;
        self.y = 0;
        self.cycle = 0;
        self.frame = 0;
        self.frame_complete = false;
        self.odd_frame = false;
    }

    fn tick(&mut self) {
        match self.y {
            -1 | 261 => self.pre_render_scanline(),
            0..=239 => self.visible_scanline(),
            240 => self.post_render_scanline(),
            241..=260 => self.vblank_scanline(),

            _ => panic!("Impossible scanline"),
        };

        self.cycle += 1;
        self.frame_complete = false;
        self.x = (self.x + 1) % 341;

        // on odd frames the last tick takes place at 0,0, replacing the idle
        // tick with the last tick of the NT fetch from 339,261.
        // that fetch is discarded by another fetch at 1,0/2,0 so there's naught
        // to do.. maybe? the NT fetches at {337-340},261 are flagged as unused.
        if self.odd_frame && self.x == 340 && self.y == -1 {
            self.x = 0;
        }

        if self.x == 0 {
            self.y += 1;
            if self.y == 262 {
                self.y = 0;
                self.frame += 1;
                self.frame_complete = true;
                self.odd_frame = !self.odd_frame;
            }
        }
    }
}

impl RP2C02 {
    fn post_render_scanline(&mut self) {
        if self.x == 1 && self.y == 241 {
            self.vblank = true;
        }
    }

    fn pre_render_scanline(&mut self) {
        if self.x == 1 && self.y == 261 {
            self.vblank = false;
            self.sprite0 = false;
            self.overflow = false;
        }

        if self.x > 0 && self.x < 257 {
            let index = self.x % 8;
            if index == 1 || index == 2 {
                // NT byte
            }
            else if index == 3 || index == 4 {
                // AT byte
            }
            else if index == 5 || index == 6 {
                // Low BG tile byte
            }
            else if index == 7 || index == 8 {
                // High BG tile byte
                if index == 7 {
                    // inc hori(v)
                }
            }
        }
    }

    fn vblank_scanline(&mut self) {

    }

    fn visible_scanline(&mut self) {
    }
}
