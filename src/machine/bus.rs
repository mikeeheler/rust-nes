use crate::ines::GamePak;
use std::fmt;

#[derive(Debug)]
pub enum BusError {
    AddressNotHandled,
    NoPakAttached,
    ReadFailure,
    WriteFailure,
}

impl<'a> fmt::Display for BusError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug)]
pub struct Bus<'a> {
    pak: Option<&'a mut GamePak>,

    /// internal, on-board memory
    /// 2KiB total, mapped in $0000-$1FFF range (mirrored in 4 blocks)
    ram: Vec<u8>,
}

impl Default for Bus<'_> {
    fn default() -> Self {
        Self {
            pak: None,
            ram: vec![0u8; 2048],
        }
    }
}

impl<'a> Bus<'a> {
    pub fn attach_pak(&mut self, pak: &'a mut GamePak) {
        self.pak = Some(pak);
    }

    pub fn reset(&mut self) {
        self.ram.fill(0);
    }

    pub fn read_dword(&self, address: u16) -> Result<u16, BusError> {
        match address {
            0..=0x1FFE => {
                let offset = usize::from(address % 0x7FF);
                let result = u16::from_le_bytes([
                    self.ram[offset],
                    self.ram[offset + 1]
                ]);

                Ok(result)
            }

            0x4020..=0xFFFF => {
                if let Some(pak) = &self.pak {
                    let lo = pak.read_word(address).unwrap();
                    let hi = pak.read_word(address + 1).unwrap();

                    Ok(u16::from_le_bytes([lo, hi]))
                }
                else {
                    Err(BusError::NoPakAttached)
                }
            }

            _ => Err(BusError::AddressNotHandled)
        }
    }

    pub fn read_word(&self, address: u16) -> Result<u8, BusError> {
        match address {
            0..=0x1FFF => {
                let offset = usize::from(address & 0x7FF);
                let result = self.ram[offset];
                Ok(result)
            },
            0x4020..=0xFFFF => {
                if let Some(pak) = &self.pak {
                    match pak.read_word(address) {
                        Ok(data) => Ok(data),
                        Err(_) => Err(BusError::ReadFailure),
                    }
                }
                else {
                    Err(BusError::NoPakAttached)
                }
            }
            _ => Err(BusError::AddressNotHandled),
        }
    }

    pub fn write_word(&mut self, address: u16, data: u8) -> Result<(), BusError> {
        match address {
            0..=0x1FFF => Ok({
                let offset = usize::from(address & 0x7FF);
                self.ram[offset] = data;
            }),
            0x6000..=0xFFFF => {
                if let Some(pak) = &mut self.pak {
                    match pak.write_word(address, data) {
                        Ok(_) => Ok(()),
                        Err(_) => Err(BusError::WriteFailure),
                    }
                }
                else {
                    Err(BusError::NoPakAttached)
                }
            }
            _ => Err(BusError::AddressNotHandled),
        }
    }

    pub fn write_dword(&mut self, address: u16, data: u16) -> Result<(), BusError> {
        match address {
            0..=0x1FFE => Ok({
                let offset = usize::from(address & 0x7FF);
                self.ram[offset] = data as u8;
                self.ram[offset + 1] = (data >> 8) as u8;
            }),
            0x4020..=0xFFFF => {
                if let Some(pak) = &mut self.pak {
                    pak.write_word(address, data as u8).unwrap();
                    pak.write_word(address + 1, (data >> 8) as u8).unwrap();
                    Ok(())
                }
                else {
                    Err(BusError::NoPakAttached)
                }
            }
            _ => Err(BusError::AddressNotHandled),
        }
    }
}
