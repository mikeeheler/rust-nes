use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::fmt;

use super::Machine;
use super::NROM;
use super::PakMap;

pub struct GamePak {
    chr_data: Vec<u8>,
    chr_file_offset: usize,

    prg_data: Vec<u8>,
    prg_file_offset: usize,
    prg_ram: Vec<u8>,

    mapper: Box<dyn PakMap>,
}

#[derive(Debug, thiserror::Error)]
pub enum GamePakError {
    #[error("Invalid header")]
    InvalidHeader,

    #[error("Truncated file")]
    TruncatedFile,

    #[error("Read error")]
    ReadError { source: std::io::Error },

    #[error("Corrupted ROM data")]
    CorruptedRomData,

    #[error(transparent)]
    IOError(#[from] std::io::Error),
}

impl GamePak {
    pub fn chr_file_offset(&self) -> usize { self.chr_file_offset }
    pub fn chr_mem_size(&self) -> usize { self.chr_data.len() }
    pub fn prg_file_offset(&self) -> usize { self.prg_file_offset }
    pub fn prg_ram_size(&self) -> usize { self.prg_ram.len() }
    pub fn prg_rom_size(&self) -> usize { self.prg_data.len() }

    pub fn dump(&self, _path: &Path) -> Result<(), &'static str> {
        /*
        let mut file = match File::create(path) {
            Ok(file) => file,
            Err(_) => return Err("Unable to create file."),
        };
        */

        for base_addr in (0x0000..self.prg_data.len()).step_by(16) {
            print!("{:0>4x}:", base_addr);
            for offset in 0..=0x0Fu8 {
                let data = self.prg_data[base_addr + usize::from(offset)];
                print!(" {:0>2x}", data);
            }
            println!();
        }

        Ok(())
    }

    pub fn load<P: AsRef<Path>>(&mut self, path: P) -> Result<Self, GamePakError> {
        let mut file = File::open(path)?;

        let mut header = [0u8; 16];
        file.read_exact(&mut header)?;

        // Verify NES header
        if &header[0..4] != b"NES\x1A" {
            return Err(GamePakError::InvalidHeader);
        }

        let prg_chunks = usize::from(header[4]);
        let chr_chunks = usize::from(header[5]);
        let flags6: u8 = header[6];
        let _flags7: u8 = header[7];
        let _flags8: u8 = header[8];
        let _flags9: u8 = header[9];
        let _flags10: u8 = header[10];
        let has_trainer = flags6 & 0b100 != 0;

        if has_trainer {
            file.seek(std::io::SeekFrom::Current(512))?; // Skip the trainer data if present
        }

        let prg_data_size = prg_chunks * 16384;
        let chr_data_size = chr_chunks * 8192;

        let mut prg_data = vec![0u8; prg_data_size];
        let prg_file_offset = file.stream_position()? as usize;
        let prg_bytes_read = file.read(&mut prg_data)?;
        if prg_bytes_read != prg_data_size {
            return Err(GamePakError::TruncatedFile);
        }

        let mut chr_data = vec![0u8; chr_data_size];
        let chr_file_offset = file.stream_position()? as usize;
        let chr_bytes_read = file.read(&mut chr_data)?;
        if chr_bytes_read != chr_data_size && chr_chunks > 0 {
            return Err(GamePakError::TruncatedFile);
        }

        // Validate data integrity
        if prg_data.iter().all(|&x| x == 0) || (chr_chunks > 0 && chr_data.iter().all(|&x| x == 0)) {
            return Err(GamePakError::CorruptedRomData);
        }

        let mapper = Box::new(NROM::new(prg_chunks));

        Ok(GamePak {
            chr_data,
            chr_file_offset,
            prg_data,
            prg_file_offset,
            prg_ram: vec![0u8; 0x2000], // 8KB of PRG RAM
            mapper,
        })
    }

    pub fn read_word(&self, address: u16) -> Result<u8, GamePakError> {
        match self.mapper.map_cpu_read(address) {
            Some(mapped_addr) => {
                if mapped_addr < self.prg_data.len() {
                    Ok(self.prg_data[mapped_addr])
                } else {
                    Err(GamePakError::TruncatedFile)
                }
            },
            None => Err(GamePakError::InvalidHeader), // Adjust error as needed
        }
    }

    pub fn write_word(&mut self, address: u16, data: u8) -> Result<(), GamePakError> {
        match self.mapper.map_cpu_write(address) {
            Some(mapped_addr) => {
                if mapped_addr < self.prg_ram.len() {
                    self.prg_ram[mapped_addr] = data;
                    Ok(())
                } else {
                    Err(GamePakError::TruncatedFile)
                }
            },
            None => Err(GamePakError::InvalidHeader), // Adjust error as needed
        }
    }

    pub fn reset(&mut self, _machine: &Machine) {
        self.prg_ram.fill(0);
        self.chr_data.fill(0);
        self.prg_data.fill(0);
    }
}

impl Default for GamePak {
    fn default() -> Self {
        Self {
            chr_data: vec![0u8; 0x2000], // Default size, 8KB
            chr_file_offset: 0,
            prg_data: vec![0u8; 0x8000], // Default size, 32KB
            prg_file_offset: 0,
            prg_ram: vec![0u8; 0x2000],  // Default size, 8KB
            mapper: Box::new(NROM::new(0)),
        }
    }
}

impl fmt::Debug for GamePak {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("GamePak")
            .field("chr_data_size", &self.chr_data.len())
            .field("chr_file_offset", &self.chr_file_offset)
            .field("prg_data_size", &self.prg_data.len())
            .field("prg_file_offset", &self.prg_file_offset)
            .field("prg_ram_size", &self.prg_ram.len())
            .field("mapper", &self.mapper.debug_info())
            .finish()
    }
}
