use super::PakMap;

pub struct NROM {
    pub prg_rom_banks: usize,
}

impl NROM {
    pub fn new(prg_rom_banks: usize) -> Self {
        Self {
            prg_rom_banks: prg_rom_banks,
            ..Self::default()
        }
    }
}

impl Default for NROM {
    fn default() -> Self {
        Self {
            prg_rom_banks: 1,
        }
    }
}

impl PakMap for NROM {
    fn map_ppu_read(&self, address: u16) -> Option<usize> {
        match address {
            0..=0x1FFF => Some(usize::from(address % 0x1FFF)),
            0x2000..=0x3EFF => Some(usize::from((address - 0x2000) % 0x1FFF)),
            0x3F00..=0x3FFF => Some(usize::from((address - 0x3F00) % 0x1F)),
            _ => None,
        }
    }

    fn map_ppu_write(&self, _address: u16) -> Option<usize> {
        None
    }

    fn map_cpu_read(&self, address: u16) -> Option<usize> {
        match address {
            // Read from RAM
            0x6000..=0x7FFF => Some(usize::from((address - 0x6000) % 0x1FFF)),
            // Read from Bank 0
            0x8000..=0xBFFF => Some(usize::from(address - 0x8000)),
            // Read either from Bank 1 if present, or from Bank 0 mirrored.
            0xC000..=0xFFFF => match self.prg_rom_banks {
                // 16KiB
                1 => Some(usize::from(address - 0xC000)),
                // 32KiB
                2 => Some(usize::from(address - 0x8000)),
                _ => None,
            }
            _ => None,
        }
    }

    fn map_cpu_write(&self, address: u16) -> Option<usize> {
        match address {
            0x6000..=0x7FFF => Some(usize::from((address - 0x6000) % 0x1FFF)),
            _ => None,
        }
    }

    fn debug_info(&self) -> String {
        format!("NROM Mapper with PRG size: {} KB", self.prg_rom_banks * 16)
    }
}
