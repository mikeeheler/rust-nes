
pub trait PakMap {
    fn map_cpu_read(&self, address: u16) -> Option<usize>;
    fn map_cpu_write(&self, address: u16) -> Option<usize>;
    fn map_ppu_read(&self, address: u16) -> Option<usize>;
    fn map_ppu_write(&self, address: u16) -> Option<usize>;

    fn debug_info(&self) -> String;
}
