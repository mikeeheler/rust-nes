#![allow(dead_code)]

use anyhow::{Context, Result};
use std::path::Path;

mod ines;
mod machine;

use ines::GamePak;
use machine::Machine;
use machine::PPU;

fn main() -> Result<()> {
    let mut vm = Machine::new();
    let nes_file_path = Path::new("./data/nestest.nes");
    let mut game_pak = GamePak::default();
    game_pak.load(&nes_file_path)
        .context(format!("Unable to load {}", nes_file_path.to_string_lossy()))?;

    println!("Successfully loaded the game pack:");
    println!("  CHR MEM: {} @ {}", game_pak.chr_mem_size(), game_pak.chr_file_offset());
    println!("  PRG ROM: {} @ {}", game_pak.prg_rom_size(), game_pak.prg_file_offset());
    println!("  PRG RAM: {}", game_pak.prg_ram_size());
    
    // vm.bus.pak.dump(&Path::new("output.txt")).unwrap();

    let mut cycle: u128 = 0;

    vm.reset().unwrap();
    vm.cpu.set_program_counter(0xC000);
    let mut last_pc = vm.cpu.program_counter();
    loop {
        // master clock is 21.477272 MHz ± 40 Hz (236.25 MHz ÷ 11)
        // CPU clock is 1.789773 MHz (master clock ÷ 12)
        // PPU clock is 5.369318 MHz (master clock ÷ 4)
        // master   0  1  2  3  4  5  6  7  8  9  A  B | C
        // CPU      0                                  | 1
        // PPU      0           1           2          | 3
        if cycle % 4 == 0 { vm.ppu.tick(); }
        if cycle % 12 == 0 {
            vm.cpu.tick(&mut vm.bus);

            if vm.cpu.is_op_complete() {
                print!("{:0<4X}  ", last_pc);
                print!("FF FF FF  INS PARAM                       ");
                print!(
                    "A:{:0<2X} X:{:0<2X} Y:{:0<2X} ",
                    vm.cpu.accumulator(),
                    vm.cpu.x_register(),
                    vm.cpu.y_register()
                );
                print!(
                    "P:{:0<2X} SP:{:0<2X} ",
                    vm.cpu.status_register(),
                    vm.cpu.stack_pointer()
                );
                print!("PPU:{} ", vm.ppu);
                println!("CYC:{}", vm.cpu.cycle_count());

                last_pc = vm.cpu.program_counter();
            }

            let error_code = vm.bus.read_word(0x0002).unwrap_or(0);
            if error_code != 0 {
                print!("non-zero 0x0002: {:0<2X}", error_code);
            }

            let error_code = vm.bus.read_word(3).unwrap_or(0);
            if error_code != 0 {
                print!("non-zero 0x0003: {:0<2X}", error_code);
            }
        }

        cycle += 1;

        if vm.ppu.frame_complete { println!("BREAK"); break; }
    }

    Ok(())
}
